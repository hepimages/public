# public

Public images

## gpu-basic-test

Basic test using an tensorflow image with python3. It checks that a GPUs is avaialble 
and runs a minimal code on it.

Can be used when setting up nodes or Panda Resources.

To use it with singularity on the GPU WN

```
singularity -s exec --nv \
docker://gitlab-registry.cern.ch/hepimages/public/gpu-basic-test \
python /test-gpu.py
```
you can also test submission to your site using prun
```
prun --exec 'python /test-gpu.py' \ 
--containerImage docker://gitlab-registry.cern.ch/hepimages/public/gpu-basic-test \
--site <your favourite PQ> --cmtConfig nvidia-gpu \
--outDS user.$RUCIO_ACCOUNT.gputest.$(date +%Y%m%d%H%M%S)
```
## gpu-atlasml-test

More complicated HP scan test that requires also an input

Can be used when setting up nodes or Panda Resources.

To use it with singularity on the GPU WN

```
singularity exec --nv --pwd /data -B <trainingfile_location>:/data \
docker://gitlab-registry.cern.ch/hepimages/public/gpu-atlasml-test \
python /btagging/DL1_c_vs_b_slim.py trainingfile.h5 10 gpu 50000
```

trainingfile.h5 file can be downloaded from rucio

```
atlasSetup
lsetup rucio
rucio get user.aforti:gpu.basic.training.h5
```

as above you can also test submission to your site using prun you have to replicate the input dataset to your site

```
prun --exec 'python /btagging/DL1_c_vs_b_slim.py trainingfile.h5 10 gpu 50000' \
--containerImage docker://gitlab-registry.cern.ch/hepimages/public/gpu-atlasml-test \
--site <your favourite PQ> --cmtConfig nvidia-gpu \
--outDS user.$RUCIO_ACCOUNT.gputest.$(date +%Y%m%d%H%M%S) \
--inDS user.aforti:gpu.basic.training.h5
```
## gpu-basic-test for PowerPC resources

Basic test using an tensorflow image with python3. It checks that a GPUs is avaialble 
and runs a minimal code on it.

To use it with singularity on the GPU WN

```
singularity -s exec --nv \
docker://gitlab-registry.cern.ch/hepimages/public/gpu-basic-test-ppc \
python /test-gpu.py
```

you can also test submission to your site using prun

```
prun --exec 'python /test-gpu.py' \ 
--containerImage docker://gitlab-registry.cern.ch/hepimages/public/gpu-basic-test-ppc \
--site <your favourite PPC PQ> --cmtConfig nvidia-gpu \
--outDS user.$RUCIO_ACCOUNT.gputest.$(date +%Y%m%d%H%M%S)
```
