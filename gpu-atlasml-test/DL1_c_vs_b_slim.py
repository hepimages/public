#!/usr/bin/env python

import h5py
import numpy as np
import keras
import sys
from keras.utils import np_utils

batch_size = int(sys.argv[4])
use_gpu = sys.argv[3]=='gpu'


import logging
logging.basicConfig(level = logging.INFO)
log = logging.getLogger('hello')
log.info("HALLO MANUEL")

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

dc =  {'GPU': 1 if use_gpu else 0, 'CPU': 1}
print('dev',dc)

config = tf.ConfigProto(device_count =  dc)
config.gpu_options.per_process_gpu_memory_fraction = 0.95
set_session(tf.Session(config=config))


trainingfilename = sys.argv[1]
epochs = int(sys.argv[2])

trainingfile = h5py.File(trainingfilename,'r')
jets = trainingfile['jets'][:]

labels = trainingfile['labels'][:]


Y_train = np_utils.to_categorical(labels.flatten(), 2)
X_train = jets

print('shuffle!')
rng_state = np.random.get_state()
np.random.shuffle(X_train)
np.random.set_state(rng_state)
np.random.shuffle(Y_train)


import keras
from keras.models import Sequential
from keras.models import model_from_json
from keras.models import load_model
from keras.layers import MaxoutDense, Dense, Dropout, Flatten, Activation
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import Adam, RMSprop
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint, EarlyStopping, History
from keras.layers.normalization import BatchNormalization

model = Sequential()

model.add(Dense(input_shape=(36,),activation="relu", units=50))

for layerSize in [50,40,30,20,10]:
    model.add(Dense(activation="relu", units=layerSize))
              
model.add(Dense(activation="softmax", units=2))

model_optimizer=Adam(lr=0.003)

print('compile')
model.compile( 
               loss='categorical_crossentropy',
               optimizer=model_optimizer,
               metrics=['accuracy'])


print('fit!')
model.fit(X_train, Y_train,batch_size=batch_size, epochs=epochs,verbose=1)
