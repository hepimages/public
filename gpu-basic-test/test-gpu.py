# Reduce the verbosity level
import os
import time 
import tensorflow as tf

# Print TF version
print('TensorFlow version: {}\n'.format(tf.__version__))

# Handling log output doesn't work that well in tensorflow 2.9.1 it either prints everything or nothing you can't put level 3 but still print the devices
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

# FAIL if there are no usable GPUs
assert tf.config.list_physical_devices('GPU'), "There are no GPU devices available or correctly configured on this host."

# Check the list of Devices
from tensorflow.python.client import device_lib
local_device_protos = device_lib.list_local_devices()
print([x.name for x in local_device_protos])

# Get used device
device_used = tf.test.gpu_device_name()

# Print the name of the Device used
print('GPU device used: {}\n'.format(device_used))

a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
b = tf.constant([7.0, 8.0, 9.0, 10.0, 11.0, 12.0], shape=[3, 2], name='b')
c = tf.matmul(a, b)
print('Vector multiplication:\n{}\n'.format(c))

# Loops on a multiplication of random generated matrices
iters = 300
n = 12000
start = time.time()

with tf.device(device_used):
    for i in range(iters):
        m1 = tf.random.normal(shape=(n, n))
        m2 = tf.random.normal(shape=(n, n))
        p = tf.matmul(m1, m2)
        if not i%100: 
            print('Printing {} {}x{} matrix product:\n{}\n'.format(i,n,n,p))
    if not (i+1)%100: 
        print('Printing {} {}x{} matrix product:\n{}\n'.format(i+1,n,n,p))
    end = time.time()
    ops = n**3 + (n-1)*n**2 # n^2*(n-1) additions, n^3 multiplications

    elapsed = (end - start)
    print('Total time elapsed: {} sec\n'.format(elapsed))
    rate = iters*ops/elapsed/10**9
    print('Each %d x %d iteration took: %.2f sec, %.2f G ops/sec' % (n, n, elapsed/iters, rate,))
